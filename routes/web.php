<?php
use Illuminate\Support\Facades\Route;

// access maklumat artikel dari larasecure.test guna AJAX
Route::view('/home', 'home');

// access post guna API
Route::view('/post', 'post');


Route::get('/', function () {
    return view('welcome');
});
