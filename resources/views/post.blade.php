<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <h2>Login</h2>
        Email : <input type="email" name="email"> <br>
        Password : <input type="password" name="password"> <br>
        <button class="btn btn-primary" id="login">Login</button>

        <span id="post"></span>
    </div>
    

    <script>
        $(function() {
            $('#login').click(function() {
                login();
            });

            function login() {
                var email = $('[name=email]').val();
                var password = $('[name=password]').val();
                $.ajax({
                    url: 'http://larasecure.test/api/login',
                    method: 'POST',
                    data: { email: email, password: password },
                    success: function(data) {
                        //alert(data);
                        console.log(data.token);
                        localStorage.setItem('token', data.token);
                        showPost();
                    },
                    error: function(err) {
                        alert(err);
                    }
                });
            }

            function showPost() {
                var token = localStorage.getItem('token');
                if (token !== '') {
                    $.ajax({
                        url: 'http://larasecure.test/api/post/all',
                        method: 'GET',
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Authorization', 'Bearer '+ token);
                        },
                        success: function(data) {
                            console.log(data);
                            //var arr = data.data;
                            var html = "<ul>";
                            $.each(data, function(index, val) {
                                html += "<li>" + val.content + "</li>";
                            });
                            html += "</ul>";
                            $('#post').html(html);
                        },
                        error: function(err) {
                            alert(err);
                        }
                    });
                }
            }
        });
    </script>
</body>
</html>